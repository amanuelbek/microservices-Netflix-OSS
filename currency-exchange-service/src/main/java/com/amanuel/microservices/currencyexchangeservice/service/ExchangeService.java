package com.amanuel.microservices.currencyexchangeservice.service;

import com.amanuel.microservices.currencyexchangeservice.ExchangeValue;
import com.amanuel.microservices.currencyexchangeservice.repository.ExchangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ExchangeService {

    @Autowired
    ExchangeRepository repository;


    public ExchangeValue getExchange(String from, String to) {
        return repository.findByFromAndTo(from, to);
    }
}

package com.amanuel.microservices.currencyexchangeservice;

import com.amanuel.microservices.currencyexchangeservice.service.ExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CurrencyExchangeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${server.port}")
    private int port;

    @Autowired
    ExchangeService service;

    @GetMapping(value = "/currency-exchange/from/{from}/to/{to}")
    public ExchangeValue retrieveExchangeValue(@PathVariable String from, @PathVariable String to){

        ExchangeValue exchange = service.getExchange(from, to);
        exchange.setPort(port);

        logger.info("{}", exchange);
        return exchange;
    }

}
